        import React, {Component} from 'react';
        import PropTypes from 'prop-types';
        // import logo from './logo.svg';
        
        import { Container, Row, Col,Jumbotron,Card, Button, CardTitle, CardText } from 'reactstrap';
        import './App.css';

        class App extends Component {
        render() {
        
        return ( 
        <div>
          <Header name={this.props.name}/>
          <Row>
            {
              this.props.players.map(function(player) {
                
                return (
              
              <Col>
              <Player name={player.name} key={player.id} />
              </Col>
              
            );
            
            })
            }
            </Row>
        </div>
        )
        }
        }
        
        App.propTypes = {
        name :PropTypes.string.isRequired,
        players : PropTypes.arrayOf(
          PropTypes.shape({
            name : PropTypes.string.isRequired,
            key : PropTypes.number.isRequired
          })
        ).isRequired 
      }


        /* Header Component  */
        class Header extends Component{
        render(){
        //console.log("inside the header ",this.props.name)
        return(
        <div>
        <Jumbotron fluid className="Header">
        <Container fluid>
        <h1 className="display-3 text-center">{this.props.name}</h1>
        </Container>
        </Jumbotron>
        </div>

        );
        }
        }
        Header.propTypes = {
        name: PropTypes.string.isRequired
        };

        
        class Player extends Component{
        render(){
            
        return (

        <div> 
          <Container>
          <Card body className="text-center">
          <CardTitle>{this.props.name}</CardTitle>
          <CardText>With supporting text below as a natural lead-in to additional content.</CardText>
          <Button>Go somewhere</Button>
          </Card>
          </Container>
        </div>

        );
        }
        }
        Player.propTypes = {
          name : PropTypes.string.isRequired
        }
        /* Player Info */
        export default App;
