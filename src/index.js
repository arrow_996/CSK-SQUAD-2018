import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import App from './App';
import Players from './players'
import registerServiceWorker from './registerServiceWorker';

ReactDOM.render(<App name="CSK-SQUAD" players={Players} />, document.getElementById('root'));
registerServiceWorker();
